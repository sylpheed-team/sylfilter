/*
 * LibSylph -- E-Mail client library
 * Copyright (C) 1999-2011 Hiroyuki Yamamoto
 */

#include "defs.h"

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "utils.h"
#include "procmsg.h"
#include "procheader.h"
#include "procmime.h"
#include "codeconv.h"

typedef struct _MsgFlagInfo {
	guint msgnum;
	MsgFlags flags;
} MsgFlagInfo;


gchar *procmsg_get_message_file_path(MsgInfo *msginfo)
{
	gchar *file;

	g_return_val_if_fail(msginfo != NULL, NULL);

	if (msginfo->encinfo && msginfo->encinfo->plaintext_file)
		file = g_strdup(msginfo->encinfo->plaintext_file);
	else if (msginfo->file_path)
		return g_strdup(msginfo->file_path);
	else
		return NULL;

	return file;
}

gchar *procmsg_get_message_file(MsgInfo *msginfo)
{
	gchar *filename = NULL;

	g_return_val_if_fail(msginfo != NULL, NULL);

	if (msginfo->file_path)
		return g_strdup(msginfo->file_path);

	return filename;
}

FILE *procmsg_open_message(MsgInfo *msginfo)
{
	FILE *fp;
	gchar *file;

	g_return_val_if_fail(msginfo != NULL, NULL);

	file = procmsg_get_message_file_path(msginfo);
	g_return_val_if_fail(file != NULL, NULL);

	if (!is_file_exist(file)) {
		g_free(file);
		file = procmsg_get_message_file(msginfo);
		if (!file)
			return NULL;
	}

	if ((fp = g_fopen(file, "rb")) == NULL) {
		FILE_OP_ERROR(file, "procmsg_open_message: fopen");
		g_free(file);
		return NULL;
	}

	g_free(file);

	if (MSG_IS_QUEUED(msginfo->flags)) {
		gchar buf[BUFFSIZE];

		while (fgets(buf, sizeof(buf), fp) != NULL)
			if (buf[0] == '\r' || buf[0] == '\n') break;
	}

	return fp;
}

FILE *procmsg_open_message_decrypted(MsgInfo *msginfo, MimeInfo **mimeinfo)
{
	FILE *fp;

	*mimeinfo = NULL;
	if ((fp = procmsg_open_message(msginfo)) == NULL)
		return NULL;
	*mimeinfo = procmime_scan_mime_header(fp);

	return fp;
}

MsgInfo *procmsg_msginfo_copy(MsgInfo *msginfo)
{
	MsgInfo *newmsginfo;

	if (msginfo == NULL) return NULL;

	newmsginfo = g_new0(MsgInfo, 1);

#define MEMBCOPY(mmb)	newmsginfo->mmb = msginfo->mmb
#define MEMBDUP(mmb)	newmsginfo->mmb = msginfo->mmb ? \
			g_strdup(msginfo->mmb) : NULL

	MEMBCOPY(msgnum);
	MEMBCOPY(size);
	MEMBCOPY(mtime);
	MEMBCOPY(date_t);

	MEMBCOPY(flags);

	MEMBDUP(fromname);

	MEMBDUP(date);
	MEMBDUP(from);
	MEMBDUP(to);
	MEMBDUP(cc);
	MEMBDUP(newsgroups);
	MEMBDUP(subject);
	MEMBDUP(msgid);
	MEMBDUP(inreplyto);

	MEMBCOPY(folder);
	MEMBCOPY(to_folder);

	MEMBDUP(xface);

	MEMBDUP(file_path);

	if (msginfo->encinfo) {
		newmsginfo->encinfo = g_new0(MsgEncryptInfo, 1);
		MEMBDUP(encinfo->plaintext_file);
		MEMBDUP(encinfo->sigstatus);
		MEMBDUP(encinfo->sigstatus_full);
		MEMBCOPY(encinfo->decryption_failed);
	}

	return newmsginfo;
}

MsgInfo *procmsg_msginfo_get_full_info(MsgInfo *msginfo)
{
	MsgInfo *full_msginfo;
	gchar *file;

	if (msginfo == NULL) return NULL;

	file = procmsg_get_message_file(msginfo);
	if (!file) {
		g_warning("procmsg_msginfo_get_full_info(): can't get message file.\n");
		return NULL;
	}

	full_msginfo = procheader_parse_file(file, msginfo->flags, TRUE);
	g_free(file);
	if (!full_msginfo) return NULL;

	full_msginfo->msgnum = msginfo->msgnum;
	full_msginfo->size = msginfo->size;
	full_msginfo->mtime = msginfo->mtime;
	full_msginfo->folder = msginfo->folder;
	full_msginfo->to_folder = msginfo->to_folder;

	full_msginfo->file_path = g_strdup(msginfo->file_path);

	if (msginfo->encinfo) {
		full_msginfo->encinfo = g_new0(MsgEncryptInfo, 1);
		full_msginfo->encinfo->plaintext_file =
			g_strdup(msginfo->encinfo->plaintext_file);
		full_msginfo->encinfo->sigstatus =
			g_strdup(msginfo->encinfo->sigstatus);
		full_msginfo->encinfo->sigstatus_full =
			g_strdup(msginfo->encinfo->sigstatus_full);
		full_msginfo->encinfo->decryption_failed =
			msginfo->encinfo->decryption_failed;
	}

	return full_msginfo;
}

gboolean procmsg_msginfo_equal(MsgInfo *msginfo_a, MsgInfo *msginfo_b)
{
	if (!msginfo_a || !msginfo_b)
		return FALSE;

	if (msginfo_a == msginfo_b)
		return TRUE;

	if (msginfo_a->folder == msginfo_b->folder &&
	    msginfo_a->msgnum == msginfo_b->msgnum &&
	    msginfo_a->size   == msginfo_b->size   &&
	    msginfo_a->mtime  == msginfo_b->mtime)
		return TRUE;

	return FALSE;
}

void procmsg_msginfo_free(MsgInfo *msginfo)
{
	if (msginfo == NULL) return;

	g_free(msginfo->xface);

	g_free(msginfo->fromname);

	g_free(msginfo->date);
	g_free(msginfo->from);
	g_free(msginfo->to);
	g_free(msginfo->cc);
	g_free(msginfo->newsgroups);
	g_free(msginfo->subject);
	g_free(msginfo->msgid);
	g_free(msginfo->inreplyto);

	slist_free_strings(msginfo->references);
	g_slist_free(msginfo->references);

	g_free(msginfo->file_path);

	if (msginfo->encinfo) {
		g_free(msginfo->encinfo->plaintext_file);
		g_free(msginfo->encinfo->sigstatus);
		g_free(msginfo->encinfo->sigstatus_full);
		g_free(msginfo->encinfo);
	}

	g_free(msginfo);
}
