/*
 * LibSylph -- E-Mail client library
 * Copyright (C) 1999-2011 Hiroyuki Yamamoto
 */

#ifndef __QUOTED_PRINTABLE_H__
#define __QUOTED_PRINTABLE_H__

#include <glib.h>

void qp_encode_line		(gchar		*out,
				 const guchar	*in);
gint qp_decode_line		(gchar		*str);

gint qp_decode_q_encoding	(guchar		*out,
				 const gchar	*in,
				 gint		 inlen);
gint qp_get_q_encoding_len	(const guchar	*str);
void qp_q_encode		(gchar		*out,
				 const guchar	*in);

#endif /* __QUOTED_PRINTABLE_H__ */
