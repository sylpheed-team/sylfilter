/*
 * LibSylph -- E-Mail client library
 * Copyright (C) 1999-2011 Hiroyuki Yamamoto
 */

#ifndef __UNMIME_H__
#define __UNMIME_H__

#include <glib.h>

gchar *unmime_header	(const gchar	*encoded_str);

#endif /* __UNMIME_H__ */
