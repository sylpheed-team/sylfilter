sylfilter (0.8-7) unstable; urgency=medium

  * debian/control
    -set Standards-Version: 4.4.0
    - use dh12
    - add Rules-Requires-Root: no
  * drop debian/.gitlab-ci.yml and use debian/salsa-ci.yml for CI
  * debian/copyright
    - use https

 -- Hideki Yamane <henrich@debian.org>  Sat, 31 Aug 2019 23:44:33 +0900

sylfilter (0.8-6) unstable; urgency=medium

  * debian/control
    - move Vcs-* to salsa.debian.org
    - set Standards-Version: 4.1.3
    - update Homepage to use https
    - set Build-Depends: debhelper (>= 11)
  * debian/compat
    - set 11
  * debian/watch
    - add pgpsigurlmangle to check PGP signature
  * debian/upstream
    - add signing-key.asc for above

 -- Hideki Yamane <henrich@debian.org>  Sun, 04 Feb 2018 10:47:30 +0900

sylfilter (0.8-5) unstable; urgency=medium

  * debian/control
    - add Section: libs for libsylfilter0
    - set Priority: optional since extra is deprecated
    - set Standards-Version: 4.1.1
    - dh10, drop unnecessary dh-autoreconf
    - also drop autotools-dev
  * debian/rules
    - drop dh-autoreconf and autotools-dev since dh10 automatically exec it
  * debian/watch
    - update to version 4
    - use https

 -- Hideki Yamane <henrich@debian.org>  Sat, 28 Oct 2017 17:14:15 +0900

sylfilter (0.8-4) unstable; urgency=medium

  * debian/control
    - add HAYASHI Kentaro <hayashi@clear-code.com> for Uploaders as upstream.
      Welcome!
    - set Standards-Version: 3.9.7

 -- Hideki Yamane <henrich@debian.org>  Thu, 31 Mar 2016 20:51:49 +0900

sylfilter (0.8-3) unstable; urgency=medium

  * debian/control
    - update Vcs-*
    - set Standards-Version: 3.9.6

 -- Hideki Yamane <henrich@debian.org>  Sun, 13 Dec 2015 17:50:40 +0900

sylfilter (0.8-2) unstable; urgency=low

  * Upload to unstable
  * debian/control
    - add Vcs-* field

 -- Hideki Yamane <henrich@debian.org>  Fri, 12 Jul 2013 10:42:55 +0900

sylfilter (0.8-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Fri, 15 Mar 2013 16:50:46 +0900

sylfilter (0.7-3) unstable; urgency=low

  * debian/rules
    - remove "-pie" for hardening
    - run dh-autoreconf to avoid build failure with -fPIC
  * debian/control
    - add "Build-Depends: dh-autoreconf"
  * debian/patches
    - add "dummyfile_config.rpath.patch", it just contains comment since 
     "dpkg-source --commit" not allow to create empty patch. This patch
     avoid configuration error without config.rpath file.

 -- Hideki Yamane <henrich@debian.org>  Sun, 13 Jan 2013 20:47:20 +0900

sylfilter (0.7-2) unstable; urgency=low

  * debian/control: upstream author pointed out now it's not tentative name,
    thus update description as well. Thanks to Hiroyuki Yamamoto! 

 -- Hideki Yamane <henrich@debian.org>  Thu, 20 Dec 2012 09:06:43 +0900

sylfilter (0.7-1) unstable; urgency=low

  * Initial release (Closes: #689844)

 -- Hideki Yamane <henrich@debian.org>  Sun, 07 Oct 2012 09:00:40 +0900
