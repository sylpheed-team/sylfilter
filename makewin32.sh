#!/bin/sh

export LIBRARY_PATH=$LIBRARY_PATH:$HOME/dist/lib:/usr/local/lib

#touch aclocal.m4 Makefile.in lib/Makefile.in src/Makefile.in configure

./configure \
  && make \
  && make install-strip
