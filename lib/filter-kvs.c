/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include <glib.h>

#include "filter-kvs.h"

struct _XFilterKVS
{
	char *dbfile;
	void *dbhandle;
};

static XFilterKVSEngine ke;


int xfilter_kvs_set_engine(XFilterKVSEngine *engine)
{
	g_return_val_if_fail(engine != NULL, -1);

	ke = *engine;

	return 0;
}

XFilterKVS *xfilter_kvs_new(const char *dbfile, void *dbhandle)
{
	XFilterKVS *kvs;

	kvs = g_new(XFilterKVS, 1);
	kvs->dbfile = g_strdup(dbfile);
	kvs->dbhandle = dbhandle;

	return kvs;
}

const char *xfilter_kvs_get_file(XFilterKVS *kvs)
{
	g_return_val_if_fail(kvs != NULL, NULL);
	return kvs->dbfile;
}

void *xfilter_kvs_get_handle(XFilterKVS *kvs)
{
	g_return_val_if_fail(kvs != NULL, NULL);
	return kvs->dbhandle;
}

XFilterKVS *xfilter_kvs_open(const char *dbfile)
{
	g_return_val_if_fail(ke.open != NULL, NULL);
	return ke.open(dbfile);
}

int xfilter_kvs_close(XFilterKVS *kvs)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.close(kvs);
}

int xfilter_kvs_insert(XFilterKVS *kvs, const char *key, void *value, int size)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.insert(kvs, key, value, size);
}

int xfilter_kvs_delete(XFilterKVS *kvs, const char *key)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.delete(kvs, key);
}

int xfilter_kvs_update(XFilterKVS *kvs, const char *key, void *value, int size)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.update(kvs, key, value, size);
}

int xfilter_kvs_fetch(XFilterKVS *kvs, const char *key, void *vbuf, int vsize)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.fetch(kvs, key, vbuf, vsize);
}

int xfilter_kvs_begin(XFilterKVS *kvs)
{
	g_return_val_if_fail(kvs != NULL, -1);
	if (ke.begin)
		return ke.begin(kvs);
	else
		return 0;
}

int xfilter_kvs_end(XFilterKVS *kvs)
{
	g_return_val_if_fail(kvs != NULL, -1);
	if (ke.end)
		return ke.end(kvs);
	else
		return 0;
}

int xfilter_kvs_get_record_size(XFilterKVS *kvs)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.size(kvs);
}

int xfilter_kvs_foreach(XFilterKVS *kvs, XFilterKVSForeachFunc func, void *data)
{
	g_return_val_if_fail(kvs != NULL, -1);
	return ke.foreach(kvs, func, data);
}

int xfilter_kvs_fetch_int(XFilterKVS *kvs, const char *key)
{
	int ival = 0;
	int size;
	char vbuf[4];

	g_return_val_if_fail(kvs != NULL, -1);

	size = xfilter_kvs_fetch(kvs, key, vbuf, 4);
	if (size == 4) {
		ival = *(gint32 *)vbuf;
		return ival;
	}

	return 0;
}

int xfilter_kvs_set_int(XFilterKVS *kvs, const char *key, int num)
{
	gint32 ival = num;
	int size;
	char vbuf[4];

	g_return_val_if_fail(kvs != NULL, -1);

	size = xfilter_kvs_fetch(kvs, key, vbuf, 4);
	if (size == 4) {
		if (num > 0)
			return xfilter_kvs_update(kvs, key, (char *)&ival, 4);
		else
			return xfilter_kvs_delete(kvs, key);
	} else if (size <= 0) {
		if (num > 0)
			return xfilter_kvs_insert(kvs, key, (char *)&ival, 4);
	}

	return -1;
}

int xfilter_kvs_increment(XFilterKVS *kvs, const char *key, int num)
{
	gint32 ival = 0;
	int size;
	char vbuf[4];

	g_return_val_if_fail(kvs != NULL, -1);

	size = xfilter_kvs_fetch(kvs, key, vbuf, 4);
	if (size == 4) {
		ival = *(gint32 *)vbuf;
		ival += num;
		return xfilter_kvs_update(kvs, key, (char *)&ival, 4);
	} else if (size <= 0) {
		ival = num;
		return xfilter_kvs_insert(kvs, key, (char *)&ival, 4);
	}

	return -1;
}

int xfilter_kvs_decrement(XFilterKVS *kvs, const char *key, int num)
{
	gint32 ival = 0;
	int size;
	char vbuf[4];

	g_return_val_if_fail(kvs != NULL, -1);

	size = xfilter_kvs_fetch(kvs, key, vbuf, 4);
	if (size == 4) {
		ival = *(gint32 *)vbuf;
		ival -= num;
		if (ival <= 0)
			return xfilter_kvs_delete(kvs, key);
		else
			return xfilter_kvs_update(kvs, key, (char *)&ival, 4);
	} else if (size <= 0) {
		return 0;
	}

	return -1;
}

static int count_func(XFilterKVS *kvs, const char *key, void *value, int size, void *data)
{
	int *sum = (int *)data;

	if (size == 4) {
		*sum += *(gint32 *)value;
	}

	return 0;
}

int xfilter_kvs_count_sum(XFilterKVS *kvs)
{
	int sum = 0;

	g_return_val_if_fail(kvs != NULL, -1);

	xfilter_kvs_foreach(kvs, count_func, &sum);
	return sum;
}
