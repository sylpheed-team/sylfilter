/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_KVS_QDBM__
#define __FILTER_KVS_QDBM__

int xfilter_kvs_qdbm_set_engine(void);

#endif /* __FILTER_KVS_QDBM__ */
