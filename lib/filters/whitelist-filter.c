/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#include <glib.h>

#include "filter.h"
#include "whitelist-filter.h"


static XFilterStatus xfilter_whitelist_func(XFilter *filter, const XMessageData *data, XFilterResult *result)
{
	return XF_NOJUNK;
}

XFilter *xfilter_whitelist_new(void)
{
	XFilter *filter;

	filter = xfilter_new(XF_TEST, "whitelist");
	xfilter_set_test_filter_func(X_TEST_FILTER(filter),
				     xfilter_whitelist_func);

	return filter;
}
