/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __XFILTER_BAYES_H__
#define __XFILTER_BAYES_H__

#include "filter.h"

typedef struct _XFilterBayesLearnStatus
{
	int junk_words;
	int nojunk_words;
	int junk_learned_num;
	int nojunk_learned_num;
} XFilterBayesLearnStatus;

XFilter *xfilter_bayes_new(void);
XFilter *xfilter_bayes_learn_junk_new(void);
XFilter *xfilter_bayes_learn_nojunk_new(void);
XFilter *xfilter_bayes_unlearn_junk_new(void);
XFilter *xfilter_bayes_unlearn_nojunk_new(void);

int xfilter_bayes_get_learn_status(XFilterBayesLearnStatus *status);
int xfilter_bayes_reset_learn_count(void);
int xfilter_bayes_reset_all(void);

int xfilter_bayes_db_show_contents(int verbose);

int xfilter_bayes_db_init(const char *path);
int xfilter_bayes_db_done(void);

#endif /* __XFILTER_BAYES_H__ */
