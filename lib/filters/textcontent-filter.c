/* SylFilter - a message filter
 *
 * Copyright (C) 2011-2012 Hiroyuki Yamamoto
 * Copyright (C) 2011-2012 Sylpheed Development Team
 */

#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>

#include "filter.h"
#include "textcontent-filter.h"

#ifdef BUILTIN_LIBSYLPH
#  include "libsylph/procheader.h"
#  include "libsylph/procmsg.h"
#  include "libsylph/procmime.h"
#  include "libsylph/utils.h"
#else
#  include <sylph/procheader.h>
#  include <sylph/procmsg.h>
#  include <sylph/procmime.h>
#  include <sylph/utils.h>
#endif

#define IS_BASE64_CHR(c)			\
	((c >= '0' && c <= '9') ||		\
	 (c >= 'A' && c <= 'Z') ||		\
	 (c >= 'a' && c <= 'z') ||		\
	 c == '+' || c == '/' || c == '=')


static void xfilter_read_body_text(GString *text, FILE *fp)
{
	char buf[8192];
	int i;

	while (fgets(buf, sizeof(buf), fp) != NULL) {
		for (i = 0; buf[i] != '\0'; i++) {
			if (!IS_BASE64_CHR(buf[i]))
				break;
		}
		if (i <= 60)
			g_string_append(text, buf);
#if 0
		else
			g_print("skip line: %s", buf);
#endif
	}
}

static XMessageData *xfilter_rfc822_to_text(const XMessageData *data)
{
	const char *file;
	MsgInfo *msginfo;
	MsgFlags flags = {0};
	MimeInfo *mimeinfo, *partinfo;
	XMessageData *newdata;
	FILE *fp, *outfp;
	GString *text;
	GPtrArray *array;
	int i;
	char *received = NULL;

	file = xfilter_message_data_get_file(data);
	if (!file)
		return NULL;

	msginfo = procheader_parse_file(file, flags, TRUE);
	if (!msginfo)
		return NULL;
	msginfo->file_path = g_strdup(file);

	mimeinfo = procmime_scan_message(msginfo);
	if (!mimeinfo) {
		procmsg_msginfo_free(msginfo);
		return NULL;
	}
	if ((fp = procmsg_open_message(msginfo)) == NULL) {
		procmime_mimeinfo_free_all(mimeinfo);
		procmsg_msginfo_free(msginfo);
		return NULL;
	}

	text = g_string_new("");

	partinfo = mimeinfo;
	while (partinfo) {
		const char *name;

		name = partinfo->filename ? partinfo->filename :
			partinfo->name ? partinfo->name : NULL;
		if (name) {
			if (text->len > 0)
				g_string_append_c(text, '\n');
			g_string_append(text, name);
			g_string_append_c(text, '\n');
		}

		if (partinfo->mime_type == MIME_TEXT ||
		    partinfo->mime_type == MIME_TEXT_HTML) {
			if (text->len > 0)
				g_string_append_c(text, '\n');
			outfp = procmime_get_text_content(partinfo, fp, NULL);
			xfilter_read_body_text(text, outfp);
			fclose(outfp);
		}
		partinfo = procmime_mimeinfo_next(partinfo);
	}

	/* get last Received: header */
	rewind(fp);
	array = procheader_get_header_array(fp, NULL);
	for (i = array->len - 1; i >= 0; i--) {
		Header *h;
		char *p, *ep;

		h = g_ptr_array_index(array, i);
		if (!g_ascii_strcasecmp(h->name, "Received")) {
			p = h->body;
			while (g_ascii_isspace(*p))
				p++;
			if (!strncmp(p, "from ", 5))
				p += 5;
			if (!strncmp(p, "by ", 3))
				p += 3;
			ep = strpbrk(p, ";\r\n");
			if (ep)
				received = g_strndup(p, ep - p);
			else
				received = g_strdup(p);
			if ((p = strstr(received, " by ")) != NULL)
				memcpy(p + 1, "  ", 2);
			if ((p = strstr(received, " with ")) != NULL)
				memcpy(p + 1, "    ", 4);
			if ((p = strstr(received, " for ")) != NULL)
				memcpy(p + 1, "   ", 3);
			if ((p = strstr(received, " id ")) != NULL) {
				memcpy(p + 1, "  ", 2);
				p += 4;
				while (*p != '\0' && !g_ascii_isspace(*p)) {
					*p++ = ' ';
				}
			}
			break;
		}
	}
	procheader_header_array_destroy(array);

	fclose(fp);
	procmime_mimeinfo_free_all(mimeinfo);

	newdata = xfilter_message_data_new(text->str, "text/plain");
	if (msginfo->from)
		xfilter_message_data_set_attribute(newdata, XM_FROM, msginfo->from, FALSE);
	if (msginfo->to)
		xfilter_message_data_set_attribute(newdata, XM_TO, msginfo->to, FALSE);
	if (msginfo->cc)
		xfilter_message_data_set_attribute(newdata, XM_CC, msginfo->cc, FALSE);
	if (msginfo->subject)
		xfilter_message_data_set_attribute(newdata, XM_SUBJECT, msginfo->subject, FALSE);
	if (received) {
		xfilter_message_data_set_attribute(newdata, XM_RECEIVED, received, FALSE);
		g_free(received);
	}

	g_string_free(text, TRUE);
	procmsg_msginfo_free(msginfo);
	return newdata;
}

static XFilterStatus xfilter_content_func(XFilter *filter, const XMessageData *data, XFilterResult *result)
{
	const char *mime_type;
	const char *content;
	XMessageData *newdata;

	g_return_val_if_fail(result != NULL, XF_ERROR);

	mime_type = xfilter_message_data_get_mime_type(data);
	if (!mime_type) {
		xfilter_result_set_status(result, XF_UNSUPPORTED_TYPE);
		return XF_UNSUPPORTED_TYPE;
	}

	if (!g_strncasecmp(mime_type, "text/", 5)) {
		content = xfilter_message_data_get_content(data);
		newdata = xfilter_message_data_new(content, "text/plain");
		xfilter_result_set_message_data(result, newdata);
	} else if (!g_strcasecmp(mime_type, "message/rfc822")) {
		newdata = xfilter_rfc822_to_text(data);
		if (!newdata) {
			xfilter_result_set_status(result, XF_ERROR);
			return XF_ERROR;
		}
		xfilter_result_set_message_data(result, newdata);
#if 0
		xfilter_debug_print("from:%s\n", xfilter_message_data_get_attribute(newdata, XM_FROM));
		xfilter_debug_print("to:%s\n", xfilter_message_data_get_attribute(newdata, XM_TO));
		xfilter_debug_print("cc:%s\n", xfilter_message_data_get_attribute(newdata, XM_CC));
		xfilter_debug_print("subject:%s\n", xfilter_message_data_get_attribute(newdata, XM_SUBJECT));
		xfilter_debug_print("received:%s\n", xfilter_message_data_get_attribute(newdata, XM_RECEIVED));
#endif
	} else {
		xfilter_result_set_status(result, XF_UNSUPPORTED_TYPE);
		return XF_UNSUPPORTED_TYPE;
	}

	xfilter_result_set_status(result, XF_REWRITTEN);
	return XF_REWRITTEN;
}

XFilter *xfilter_textcontent_new(void)
{
	XFilter *filter;

	filter = xfilter_new(XF_CONTENT, "text-content");
	xfilter_set_content_filter_func(X_CONTENT_FILTER(filter),
					xfilter_content_func);

	return filter;
}
