/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

/* filter.h */

#ifndef __FILTER_H__
#define __FILTER_H__

typedef enum {
	XF_APP_MODE_STANDALONE = 0,
	XF_APP_MODE_EXT_LIBSYLPH = 1
} XFilterAppMode;

typedef enum {
	XF_CONTENT,
	XF_TEST
} XFilterType; 

typedef enum {
	XF_NOJUNK,
	XF_JUNK,
	XF_UNCERTAIN,
	XF_REWRITTEN,
	XF_NONE,
	XF_UNSUPPORTED_TYPE,
	XF_ERROR
} XFilterStatus;

typedef enum {
	XM_FROM,
	XM_TO,
	XM_CC,
	XM_SUBJECT,
	XM_RECEIVED,
} XMessageAttr;

typedef struct _XFilterManager	XFilterManager;
typedef struct _XFilter		XFilter;
typedef struct _XContentFilter	XContentFilter;
typedef struct _XTestFilter	XTestFilter;
typedef struct _XMessageData	XMessageData;
typedef struct _XFilterResult	XFilterResult;

#define XFILTER(f)		((XFilter *)f)
#define X_CONTENT_FILTER(f)	((XContentFilter *)f)
#define X_TEST_FILTER(f)	((XTestFilter *)f)

typedef XFilter * (*XFilterConstructorFunc)	(void);

typedef XFilterStatus (*XContentFilterFunc)	(XFilter *filter,
						 const XMessageData *data,
						 XFilterResult *result);
typedef XFilterStatus (*XTestFilterFunc)	(XFilter *filter,
						 const XMessageData *data,
						 XFilterResult *result);


int	 xfilter_init	(XFilterAppMode mode);
void	 xfilter_done	(void);

int	 xfilter_get_app_mode(void);

/* XFilter */

XFilter *xfilter_new	(XFilterType type, const char *name);
void	 xfilter_free	(XFilter *filter);

XFilterType xfilter_get_type(XFilter *filter);
const char *xfilter_get_name(XFilter *filter);

void xfilter_set_input_mime_types(XFilter *filter, const char **types);
void xfilter_set_output_mime_type(XFilter *filter, const char *type);

/* XMessageData */

XMessageData *xfilter_message_data_new(const char *src, const char *mime_type);
XMessageData *xfilter_message_data_read_file(const char *file, const char *mime_type);
void	 xfilter_message_data_free(XMessageData *msgdata);
void	 xfilter_message_data_set_file(XMessageData *msgdata, const char *file);
void	 xfilter_message_data_set_content(XMessageData *msgdata, char *content);
void	 xfilter_message_data_set_attribute(XMessageData *msgdata, XMessageAttr attr, const char *value, int append);
void	 xfilter_message_data_copy_attributes(XMessageData *msgdata, const XMessageData *srcdata);
const char *xfilter_message_data_get_mime_type(const XMessageData *msgdata);
const char *xfilter_message_data_get_file(const XMessageData *msgdata);
const char *xfilter_message_data_get_content(const XMessageData *msgdata);
const char *xfilter_message_data_get_attribute(const XMessageData *msgdata, XMessageAttr);

/* X*Filter */

void	 xfilter_set_content_filter_func(XContentFilter *filter, XContentFilterFunc func);
void	 xfilter_set_test_filter_func(XTestFilter *filter, XTestFilterFunc func);

XFilterStatus xfilter_exec(XFilter *filter, const XMessageData *msgdata, XFilterResult *result);

/* XFilterResult */

void	 xfilter_result_print(XFilterResult *result);

XFilterResult *xfilter_result_new(void);
void xfilter_result_set_status(XFilterResult *result, XFilterStatus status);
void xfilter_result_set_message_data(XFilterResult *result, XMessageData *msgdata);
void xfilter_result_set_probability(XFilterResult *result, double prob);
XFilterStatus xfilter_result_get_status(XFilterResult *result);
XMessageData *xfilter_result_get_message_data(XFilterResult *result);
double xfilter_result_get_probability(XFilterResult *result);
void	 xfilter_result_free(XFilterResult *result);

/* Utility functions */

void	 xfilter_set_debug_mode(int mode);
int	 xfilter_get_debug_mode(void);
void	 xfilter_debug_print(const char *format, ...);

#endif /* __FILTER_H__ */
