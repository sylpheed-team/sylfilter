/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_KVS_H__
#define __FILTER_KVS_H__

typedef struct _XFilterKVS	XFilterKVS;

typedef int (*XFilterKVSForeachFunc) (XFilterKVS *kvs, const char *key, void *value, int size, void *data);

typedef struct _XFilterKVSEngine
{
	XFilterKVS * (*open)	(const char *dbfile);
	int (*close)		(XFilterKVS *kvs);
	int (*insert)		(XFilterKVS *kvs, const char *key, void *value,
				 int size);
	int (*delete)		(XFilterKVS *kvs, const char *key);
	int (*update)		(XFilterKVS *kvs, const char *key, void *value,
				 int size);
	int (*fetch)		(XFilterKVS *kvs, const char *key, void *vbuf, int vsize);
	int (*begin)		(XFilterKVS *kvs);
	int (*end)		(XFilterKVS *kvs);
	int (*size)		(XFilterKVS *kvs);
	int (*foreach)		(XFilterKVS *kvs, XFilterKVSForeachFunc func, void *data);
} XFilterKVSEngine;

int xfilter_kvs_set_engine	(XFilterKVSEngine *engine);

XFilterKVS *xfilter_kvs_new	(const char *dbfile, void *dbhandle);
const char *xfilter_kvs_get_file(XFilterKVS *kvs);
void *xfilter_kvs_get_handle	(XFilterKVS *kvs);

XFilterKVS *xfilter_kvs_open	(const char *dbfile);
int xfilter_kvs_close		(XFilterKVS *kvs);

int xfilter_kvs_insert		(XFilterKVS *kvs, const char *key, void *value,
				 int size);
int xfilter_kvs_delete		(XFilterKVS *kvs, const char *key);
int xfilter_kvs_update		(XFilterKVS *kvs, const char *key, void *value,
				 int size);
int xfilter_kvs_fetch		(XFilterKVS *kvs, const char *key, void *vbuf, int vsize);
int xfilter_kvs_begin		(XFilterKVS *kvs);
int xfilter_kvs_end		(XFilterKVS *kvs);

int xfilter_kvs_get_record_size	(XFilterKVS *kvs);

int xfilter_kvs_foreach		(XFilterKVS *kvs, XFilterKVSForeachFunc func, void *data);

int xfilter_kvs_fetch_int	(XFilterKVS *kvs, const char *key);
int xfilter_kvs_set_int		(XFilterKVS *kvs, const char *key, int num);

int xfilter_kvs_increment	(XFilterKVS *kvs, const char *key, int num);
int xfilter_kvs_decrement	(XFilterKVS *kvs, const char *key, int num);

int xfilter_kvs_count_sum	(XFilterKVS *kvs);

#endif /* __FILTER_KVS_H__ */
