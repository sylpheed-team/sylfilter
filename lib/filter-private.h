/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_PRIVATE_H__
#define __FILTER_PRIVATE_H__

#include "filter.h"

struct _XFilterManager
{
	XFilter *filter_list;

	XMessageData *data;

	char *original_encoding;
};

struct _XFilter
{
	XFilterType type;
	char *name;
	char *input_mime_types[8];
	char *output_mime_type;

	XFilterManager *manager;
	XFilter *next;
};

struct _XContentFilter
{
	XFilter filter;

	XFilterStatus (*content_filter_func) (XFilter *filter, const XMessageData *data, XFilterResult *result);
};

struct _XTestFilter
{
	XFilter filter;

	XFilterStatus (*test_filter_func)    (XFilter *filter, const XMessageData *data, XFilterResult *result);
};

struct _XMessageData
{
	char *mime_type;

	char *file;
	char *content;

	char *from;
	char *to;
	char *cc;
	char *subject;

	char *received;
};

struct _XFilterResult
{
	XFilterStatus status;
	XMessageData *msgdata;
	double probability;
};

#endif /* __FILTER_PRIVATE_H__ */
