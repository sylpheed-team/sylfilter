/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_MANAGER_H__
#define __FILTER_MANAGER_H__

#include "filter.h"

XFilterManager *xfilter_manager_new	(void);

void	 xfilter_manager_filter_add	(XFilterManager *mgr, XFilter *filter);
void	 xfilter_manager_filter_remove	(XFilterManager *mgr, XFilter *filter);

int	 xfilter_manager_add_filters	(XFilterManager *mgr, XFilterConstructorFunc ctors[]);

XFilterResult *xfilter_manager_run	(XFilterManager *mgr, XMessageData *msgdata);

/* clears only internal state */
void	 xfilter_manager_done		(XFilterManager *mgr);

void	 xfilter_manager_free		(XFilterManager *mgr);

#endif /* __FILTER_MANAGER_H__ */
