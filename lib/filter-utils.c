/* SylFilter - a message filter
 *
 * Copyright (C) 2011-2012 Hiroyuki Yamamoto
 * Copyright (C) 2011-2012 Sylpheed Development Team
 */

#include "config.h"

#include <glib.h>

#ifdef BUILTIN_LIBSYLPH
#  include "libsylph/utils.h"
#else
#  include <sylph/utils.h>
#endif

#ifdef G_OS_WIN32
#  include <windows.h>
#  include <wchar.h>
#  include <shlobj.h>
#endif

#include "filter.h"
#include "filter-utils.h"


char *xfilter_utils_get_file_contents(const char *file)
{
	char *contents = NULL;

	g_return_val_if_fail(file != NULL, NULL);

	if (g_file_get_contents(file, &contents, NULL, NULL))
		return contents;
	else
		return NULL;
}

void xfilter_free_mem(void *mem)
{
	g_free(mem);
}

const char *xfilter_utils_get_home_dir(void)
{
	return get_home_dir();
}

static char *base_dir = NULL;

const char *xfilter_utils_get_base_dir(void)
{
	if (!base_dir) {
		base_dir = g_strdup(xfilter_utils_get_default_base_dir());
	}

	return base_dir;
}

int xfilter_utils_set_base_dir(const char *path)
{
	const char *tmpdir;
	const char *rcdir;

#ifdef G_OS_WIN32
	if (path) {
		char *upath = g_locale_to_utf8(path, -1, NULL, NULL, NULL);
		if (xfilter_utils_mkdir(upath) < 0) {
			g_free(upath);
			return -1;
		}

		if (base_dir)
			g_free(base_dir);
		base_dir = upath;
	} else {
		path = xfilter_utils_get_default_base_dir();

		if (xfilter_utils_mkdir(path) < 0)
			return -1;

		if (base_dir)
			g_free(base_dir);
		base_dir = g_strdup(path);
	}
#else
	if (!path)
		path = xfilter_utils_get_default_base_dir();

	if (xfilter_utils_mkdir(path) < 0)
		return -1;

	if (base_dir)
		g_free(base_dir);
	base_dir = g_strdup(path);
#endif

	if (xfilter_get_app_mode() == XF_APP_MODE_STANDALONE) {
		set_rc_dir(base_dir);
	} else {
		/* if default rc dir not exist, use sylfilter base dir */
		rcdir = get_rc_dir();
		if (!is_dir_exist(rcdir)) {
			set_rc_dir(base_dir);
		}
	}

	tmpdir = get_tmp_dir();
	xfilter_utils_mkdir(tmpdir);

	return 0;
}

static const char *xfilter_utils_get_appdata_dir(void)
{
#ifdef G_OS_WIN32
	static char *appdata_dir = NULL;

	if (appdata_dir)
		return appdata_dir;
	else {
		HRESULT hr;
		wchar_t path[MAX_PATH + 1];

		hr = SHGetFolderPathW(NULL, CSIDL_APPDATA, NULL, 0, path);
		if (hr == S_OK)
			appdata_dir = g_utf16_to_utf8(path, -1, NULL, NULL, NULL);
		else
			return g_get_home_dir();
	}

	return appdata_dir;
#else
	return g_get_home_dir();
#endif
}

const char *xfilter_utils_get_default_base_dir(void)
{
	static char *default_base_dir = NULL;

	if (default_base_dir)
		return default_base_dir;
	else {
		const char *parent;

		parent = xfilter_utils_get_appdata_dir();
		default_base_dir = g_strconcat(parent, G_DIR_SEPARATOR_S,
#ifdef G_OS_WIN32
					       "SylFilter"
#else
					       ".sylfilter"
#endif
					       , NULL);
	}

	return default_base_dir;
}

int xfilter_utils_mkdir(const char *path)
{
	if (is_dir_exist(path))
		return 0;

	return g_mkdir(path, 0700);
}

static GHashTable *conf_table = NULL;

static void kv_destroy_func(gpointer data)
{
	g_free(data);
}

void xfilter_set_conf_value(const char *key, const char *value)
{
	if (!conf_table)
		conf_table = g_hash_table_new_full(g_str_hash, g_str_equal, kv_destroy_func, kv_destroy_func);

	g_hash_table_replace(conf_table, g_strdup(key), g_strdup(value));
}

const char *xfilter_get_conf_value(const char *key)
{
	if (!conf_table)
		return NULL;
	return (char *)g_hash_table_lookup(conf_table, key);
}

void xfilter_conf_value_clear(void)
{
	if (conf_table) {
		g_hash_table_destroy(conf_table);
		conf_table = NULL;
	}
}
