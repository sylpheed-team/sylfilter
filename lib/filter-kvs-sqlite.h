/* SylFilter - a message filter
 *
 * Copyright (C) 2011 Hiroyuki Yamamoto
 * Copyright (C) 2011 Sylpheed Development Team
 */

#ifndef __FILTER_KVS_SQLITE__
#define __FILTER_KVS_SQLITE__

int xfilter_kvs_sqlite_set_engine(void);

#endif /* __FILTER_KVS_SQLITE__ */
